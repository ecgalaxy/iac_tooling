def test_commands(host):
  cmd = host.run('aws --version')
  assert cmd.rc == 0

  cmd = host.run('git --version')
  assert cmd.rc == 0

  cmd = host.run('java -version')
  assert cmd.rc == 0

  cmd = host.run('jq --version')
  assert cmd.rc == 0

  cmd = host.run('mvn --version')
  assert cmd.rc == 0

  cmd = host.run('node --version')
  assert cmd.rc == 0

  cmd = host.run('summon --version')
  assert cmd.rc == 0

  cmd = host.run('task --version')
  assert cmd.rc == 0

  cmd = host.run('terraform --version')
  assert cmd.rc == 0
